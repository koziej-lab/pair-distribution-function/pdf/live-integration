#!/bin/bash
#SBATCH --partition=allcpu      #Partition to work on (select Cluster)
#SBATCH --time=24:00:00		 #Maximum time of computation now set at 24 h
#SBATCH --nodes=1            #For multiple nodes you need a script that runs as MPI
#SBATCH	--job-name=integration_test           #Name of the job to find it in queue or in 
#SBATCH --mail-type=END,FAIL                   #Send an email when finished or failed 
#SBATCH --mail-user=lian.belgardt@physnet.uni-hamburg.de      #Send to email adress

#use underscores here or you will have problems
NPROCS=$(nproc)
path_raw_data="/asap3/petra3/gpfs/p21.1/2024/data/11018943/raw/pe/" #directory to be integrated
fpath_poni="/asap3/petra3/gpfs/p21.1/2024/data/11018943/processed/_People/Tjark/Poni/SDD_400mm.poni"
fpath_mask="/asap3/petra3/gpfs/p21.1/2024/data/11018943/processed/_People/Tjark/Mask/SDD_400mm.edf"
path_integrated_data="/asap3/petra3/gpfs/p21.1/2024/data/11018943/processed/_People/Tjark/postprocessing/SDD400/" #directory to save integrated data 

plarization=0.99
number_of_points=3000
unit="q_A^-1"


module load maxwell
module load conda/3.11
activate XrayDiff
cd  /asap3/petra3/gpfs/p21.1/2024/data/11018943/processed/_People/Tjark/
python3 maxwell_integrate_with_subdirs.py $path_raw_data $path_integrated_data $fpath_poni $fpath_mask $NPROCS $plarization $number_of_points $unit #Start python script
