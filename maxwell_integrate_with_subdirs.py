
#!/usr/bin/env python3
import pyFAI
import fabio
import os
import sys
from time import sleep
from glob import glob
import logging
logger = logging.getLogger()
logger.setLevel(logging.CRITICAL)
import threading
from watchdog.observers.polling import PollingObserver
from watchdog.events import PatternMatchingEventHandler
from multiprocessing.pool import ThreadPool as Pool

global NPROC


def integrate_ims_in_dir(path_im, path_int, dtype_im=".tif", dtype_int=".dat"):
    """
    Azimuthally integrate all images in directory <path> with ending <dtype_im>
    to patterns of ending <dtype_int> if not already integrated.
    :param 'str' path: path to directory where to apply the azimuthal integration
    :param 'str' dtype_im: data type/filename ending of image file
    :param 'str' dtype_int: data type/filename ending of pattern file
    """
    global NPROC
    fnames_ims = []#= glob(os.path.join(path_im, "*" + dtype_im))
    path_int_list = []
    for path, subdirs, files in os.walk(path_im):
        for name in files:
            if ("cu" not in name) or ("Cu" not in name) or ("np" not in name):
                if "sdd500" not in name and "sdd750" not in name and "sdd1000" not in name:
                    fnames_ims.append(os.path.join(path, name))
                    if path_im != str(path):
                        path_new = str(path).replace(path_im,'')      
                        path_new = path_int + path_new 
                    else:
                        path_new = path_int 
                    path_int_list.append(path_new)

    #fnames_ims.sort(key=str.lower)

    def integration_thread(fname_im,path_int):
        global NPT
        global UNIT
        global POLARIZATION
        
        if not os.path.isdir(path_int):
            os.mkdir(path_int)
        im = fabio.open(fname_im).data
        basename_int = os.path.basename(fname_im)[:-len(dtype_im)] + dtype_int
        fname_int = os.path.join(path_int, basename_int)
        if not os.path.isfile(fname_int):
            ai.integrate1d(data=im,
                           npt=NPT,
                           filename=fname_int,
                           mask=mask,
                           polarization_factor=POLARIZATION,
                           correctSolidAngle=True,
                           error_model='azimuthal',
                           unit=UNIT)
            print(f"integrate: {fname_im}")
 
    pool = Pool(int(NPROC))
    
    for i,fname_im in enumerate(fnames_ims):
        pool.apply_async(integration_thread, (fname_im,path_int_list[i]))

    pool.close()
    pool.join()
    
        
def integrate_on_created(event, path_int, dtype_im=".tif", dtype_int=".dat"):
    """
    Azimuthally integrate all created images in directory <path>
    with ending <dtype_im> to patterns of ending <dtype_int>.
    :param 'watchdog.events.FileCreatedEvent' event: watchdog object to check for created files
    :param 'str' dtype_im: data type/filename ending of image file
    :param 'str' dtype_int: data type/filename ending of pattern file
    """
        
    if not os.path.isdir(path_int):
        os.mkdir(path_int)

    if event.src_path[-len(dtype_im):] == dtype_im:
        im = fabio.open(event.src_path).data
        basename_int = os.path.basename(event.src_path)[:-len(dtype_im)] + dtype_int
        ai.integrate1d(data=im,
                        npt=3000,
                        filename=os.path.join(path_int, basename_int),
                        mask=mask,
                         polarization_factor=0.95,
                        correctSolidAngle=True,
                        error_model='azimuthal',
                        unit="q_A^-1")
        print(f"integrate: {event.src_path}")


class Handler(PatternMatchingEventHandler):
    patterns = ["*tif","*tiff","*TIF","*TIFF"]
    ignore_patterns = []
    ignore_directories = True
    case_sensitive = True
    go_recursively = True
    
    def __init__(self, path_im, path_int):
            PatternMatchingEventHandler.__init__(self)
            self.path_im = path_im
            self.path_int = path_int

    def on_created(self, event):
        #wait that the transfer of the file is finished before processing it
        path_event = str(os.path.dirname(event.src_path))
        print(path_event)
        if self.path_im != path_event:
            if self.path_im in path_event:
                path_event = path_event.replace(self.path_im,'')
                path_int = self.path_int + path_event
        else:
            path_int = self.path_int
        integrate_on_created(event,path_int)
             

if __name__ == '__main__':
    path_im=sys.argv[1]
    path_int=sys.argv[2]
    fname_poni=sys.argv[3]
    fname_mask=sys.argv[4]
    NPROC=sys.argv[5]
    POLARIZATION=sys.argv[6]
    NPT=sys.argv[7]
    UNIT=sys.argv[8]

    
    
    if not os.path.isdir(path_int):
        os.mkdir(path_int)

    ai = pyFAI.load(fname_poni)
    mask = fabio.open(fname_mask).data if fname_mask else None
    
    integrate_ims_in_dir(path_im, path_int)

    print("Observing directory:  " +str(path_im))

    event_handler = Handler(path_im,path_int)
    observer = PollingObserver()
    observer.schedule(event_handler, path_im, recursive=True)
    print("About to start observer")
    observer.start()

    try:
        while True:
            sleep(0.05)
    except KeyboardInterrupt:
        observer.stop()
        observer.join()
